
/*
#include <vector>
#include <math.h>

class Real {
public:
  Real( double v ) : m_value(v) { };
  operator double&() { return m_value; }
  Real operator = (double y)
  {
    m_value = y;
    return(*this);
  }
  double operator ^ (double y)
  {
    return(pow(m_value,y));
  }
private:
  double m_value;
};

*/

double pow(double z,double x);

////////////////////////////////////////////////////////////////////////////////
//Target function defined as simple code
double my_target(const double* x, double* grad)
////////////////////////////////////////////////////////////////////////////////
{
  double _05 = 1.0/2.0;
  double y = pow(x[1], _05 );
  double z = pow(x[1],-_05)*_05;
//printf("TRACE [target.eval] x[1]=%.15lg; y=%.15lg; z=%.15lg\n", x[1], y, z);
  if(grad) 
  {
    grad[0] = 0.0;
    grad[1] = z;
  };
//printf("TRACE [target.eval] x=(%.15lg,%.15lg) f=%.15lg grad=(%.15lg,%.15lg)\n",x[0],x[1],f,grad[0],grad[1]); 
  return(y);
};



////////////////////////////////////////////////////////////////////////////////
//When a set of constraining functions is part of a parametric family
//of functions we can define a class to facilitate the definition
class my_inequation 
////////////////////////////////////////////////////////////////////////////////
{
public:
  double a;
  double b;

  my_inequation(double a_, double b_)
  : a(a_),
    b(b_)
  {}

  double eval(const double* x, double* grad)
  {
    const double& x1 = x[0];
    const double& x2 = x[1];
    double ax1b = a*x1 + b;
    if(grad) {
    //printf("TRACE [target.eval] HAS GRADIENT\n");
      grad[0] = 3 * a * pow(ax1b,2);
      grad[1] = -1.0;
    };
    double g = pow(ax1b,3) - x2;
  //printf("TRACE [my_inequation.eval] a=%.15lg b=%.15lg x=(%.15lg,%.15lg) g=%.15lg  grad=(%.15lg,%.15lg)\n",a,b,x[0],x[1],g,grad[0],grad[1]); 
    return(g);
  }

};

