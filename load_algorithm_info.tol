//////////////////////////////////////////////////////////////////////////////
// FILE   : load_algorithm_info.tol
// PURPOSE: Loads algorithm info from doc/NLopt_algorithms.xls
//////////////////////////////////////////////////////////////////////////////

/// #Require NonLinGloOpt;
/// #Require TolExcel;

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member._load_algorithm_info = 
"Loads algorithm info from doc/NLopt_algorithms.xls";
Set _load_algorithm_info(Real void)
//////////////////////////////////////////////////////////////////////////////
{
  Real Eval("Real {
    #Require TolExcel;
  1}");
  Text path = GetAbsolutePath("./");
  WriteLn("TRACE [_load_algorithm_info] 1 path:'"+path+"'"); 
  Text filePath = path+"doc/NLopt_algorithms.xls";
  NameBlock xls = Eval("TolExcel::@WorkBook::Open(filePath);");
  Text hoja = "NLopt Algorithms";
  Real xls::ActivateNamedWS(hoja);

  Set header = Copy(Empty);
  
  Text getCellText(Real row, Real col)
  {
    xls::ReadText(row,col)
  };
  Real getCellReal(Real row, Real col)
  {
    Anything cell = xls::ReadCell(row,col);
    Text gra = Grammar(cell);
    If(gra=="Real", cell, ?)
  };

  Text prfx = "NonLinGloOpt::AnalyticalClass::";
  Set algInf = For(2, xls::_.maxRow, NonLinGloOpt::@AlgorithmInfo(Real row)
  {
    Text name = getCellText(row,1);
    Set str = NonLinGloOpt::@AlgorithmInfo
    (
      Real Identifier           = getCellReal(row, 2),
      Text MainCode             = getCellText(row, 3),
      Text MainAlgorithmName    = getCellText(row, 4),
      Text AnalyticalClass      = getCellText(row, 5),
      Real AnalyticalClassId    = Eval(prfx+AnalyticalClass),
      Real IsGlobal             = getCellReal(row, 6),
      Real NeedsGradient        = getCellReal(row, 7),
      Real NeedsFeasiblePoint   = getCellReal(row, 8),
      Real NeedsBounds          = getCellReal(row, 9),
      Real SupportsBounds       = getCellReal(row,10),
      Real SupportsInequalities = getCellReal(row,11),
      Real SupportsEqualities   = getCellReal(row,12),
      Real HasSubsidiaryMethod  = getCellReal(row,13),
      Text IsSuperseded         = getCellText(row,14),
      Real MinimumDimension     = getCellReal(row,15),
      Text PreferredScale       = getCellText(row,16)
    );
    Eval(name+"=str")
  });
  Real xls::Close(?);
  Sort(algInf,Real(NonLinGloOpt::@AlgorithmInfo a, NonLinGloOpt::@AlgorithmInfo b)
  {
    Compare(a->Identifier,b->Identifier)
  }) 

};

/// Real Ois.Store(_load_algorithm_info(?),"algorithm_info.oza");
Set _.algorithmInfo = AvoidErr.NonDecAct(_load_algorithm_info(?));

/* */

